/*
Скрипт для тестирования студентов КубГУ
*/

let questions1 = ["question1", "question2", "question3"];
let indexOfQuestion = 0;
let countOfAnswer = 0;

class Question{
    constructor(question, answer1, answer2, answer3, answer4, answerTrue)
    {
        this.question = question;
        this.answer1 = answer1;
        this.answer2 = answer2;
        this.answer3 = answer3;
        this.answer4 = answer4;
        this.answerTrue = answerTrue;
    }
    
    getAnswer(){
        return this.answerTrue;
    }
}

let questions = [];
for(i = 0; i < 10; i++){
    var rnd =  Math.floor(Math.random() * 4) + 1;
    questions.push(new Question("question" + i + " " + rnd, "1"+i, "2"+i, "3"+i, "4"+i, rnd));
}

startTest();

function startTest(){
    try
    {
        $("#zero_block").after('<h2 id = "text_question" class="question item wow fadeInDown">'+questions[0].question+'</h2>');
        $("#text_question").after('<div class="question" id="one">'+
                '<div class="item wow fadeInDown">'+
                    '<button id = "ans1" class="btn_answer" onClick="swapQuestion(1)">'
                        +questions[indexOfQuestion].answer1+'</button>'+
                    '<button id = "ans2" class="btn_answer" onClick="swapQuestion(2)">'
                        +questions[indexOfQuestion].answer2+'</button>'+
                    '<button id = "ans3" class="btn_answer" onClick="swapQuestion(3)">'
                        +questions[indexOfQuestion].answer3+'</button>'+
                    '<button id = "ans4" class="btn_answer" onClick="swapQuestion(4)">'
                        +questions[indexOfQuestion].answer4+'</button>'+
                '</div>'+
            '</div>');
    }
    catch (e){
        console.log("Haven't questions");
    }
}

function getQuestion(){
    var url = "https://script.google.com/macros/s/AKfycbznGxJ9-PLZAfVyWRm1XF0h8oltLMO9OfODzd2noGNUt9Zi5Fl8Cc7V7Q7Q_FBUvQs2Tw/exec";
    var mode = 'no-cors'
    fetch(url, {mode:'no-cors', method: 'GET' }) 
        .then(function (response) {  
              promise1 = response.text()
              promise1.then((value) => {
                  PrintLog(value);
                  console.log(value);  
                  return alert(value);  // Will respond asynchronously.
              });
        })
    .catch(function (error){
        console.log("!!Error!! ", error);
    });
    return true;
}

function PrintLog(str){
    console.log(str);
}

function swapQuestion(index){
    indexOfQuestion+=1;
    if (index == questions[indexOfQuestion - 1].getAnswer()){
        countOfAnswer++;
        console.log("True");
    }
    else{
        console.log(questions[indexOfQuestion - 1].getAnswer() + " " + index);
    }
    getQuestion();
    if (indexOfQuestion < questions.length){
        
        document.getElementById('text_question').textContent = questions[indexOfQuestion].question;
        document.getElementById('ans1').textContent = questions[indexOfQuestion].answer1;
        document.getElementById('ans2').textContent = questions[indexOfQuestion].answer2;
        document.getElementById('ans3').textContent = questions[indexOfQuestion].answer3;
        document.getElementById('ans4').textContent = questions[indexOfQuestion].answer4;
    }
    else
    {
        console.log(countOfAnswer);
        document.getElementById('text_question').remove();
        document.getElementById('one').remove();
    }
}

function sendResult(){
    var url = "https://script.google.com/macros/s/AKfycbx-cd54Iit2DXiSg0dicpwGl_nIqj-5513uvz4O7N0gGHlbYZlBhPWqX-yOaKoVFfX6FA/exec";
      fetch(url,{
        method: 'POST',
        mode: 'no-cors',
        headers: {
              'type': 'application/json',
              'Content-Type': 'application/json',
              'cache-control': 'no-cache',
          },
        body: JSON.stringify({
            'monitorURL': '1',
            'alertType': 'alertType',
            'alertDetails': '3'
          })
      })
      .then('json')  
      .then(function (data) {  
        console.log('Request succeeded with JSON response', data);  
      })  
      .catch(function (error) {  
        console.log('Request failed', error);  
      })
      

      return alert('d');  // Will respond asynchronously.
}
